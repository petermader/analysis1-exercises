# Analysis I -- Blatt 3

## Aufgabe 1

(1) (a) **Induktionsanfang**: mit $n = 1$ gilt
        $$\sum_{k=1}^n k^3 = 1^3 = 1 = \frac{1^2(1+1)^2}{4} =
        \frac{n^2(n+1)^2}{4}.$$

        **Induktionsvoraussetzung**: es sei $n > 1$ und die Behauptung gelte für
        $n - 1$.

        **Induktionsschritt**: unter Anwendung der Induktionsvoraussetzung gilt
        $$\sum_{k=1}^n = n^3 + \sum_{k=1}^{n-1} k^3 = n^3 + \frac{(n-1)^2n^2}{4}
        = \frac{4n^3 + (n-1)^2n^2}{4} = \frac{n^2(4n + (n-1)^2)}{4} =
        \frac{n^2(n+1)^2}{4}.$$
    (b) Es gilt
        $$\pmat{n+1\\2} = \frac{(n+1)!}{2!(n-1)!} = \frac{n(n+1)}{2}.
        \tag{$*$}$$

        **Induktionsanfang**: mit $n = 1$ gilt
        $$\sum_{k=1}^n (-1)^kk^2 = -1 = (-1)^1\pmat{2\\2} =
        (-1)^n\pmat{n+1\\2}.$$

        **Induktionsvoraussetzung**: es sei $n > 1$ und die Behauptung gelte für
        $n-1$.

        **Induktionsschritt**: unter Anwendung der Induktionsvoraussetzung gilt
        $$\begin{aligned}
        \sum_{k=1}^n (-1)^kk^2 &= (-1)^nn^2 + \sum_{k=1}^{n-1} (-1)^kk^2 =
        (-1)^nn^2 + (-1)^{n-1}\pmat{n\\2}\\
        &= (-1)^n\left(n^2 -
        \frac{n(n-1)}{2}\right) = (-1)^n\frac{n(n+1)}{2} =
        (-1)^n\pmat{n+1\\2}.
        \end{aligned}$$
(2) (a) **Induktionsanfang**: mit $n = 1$ und $a \in \N$ gilt
        $$f(a^n) = f(a^1) = f(a) = 1 \cdot f(a) = n f(a).$$

        **Induktionsvoraussetzung**: die Behauptung gelte für ein festes $n$.

        **Induktionsschritt**: unter Anwendung der Induktionsvoraussetzung gilt
        $$f(a^{n+1}) = f(a^na) = f(a^n) + f(a) = nf(a) + f(a) = (n+1)f(a).$$
    (b) Unter Anwendung von (a) gilt
        $$f(2^{f(3)}) = f(3)f(2) = f(2)f(3) = f(3^{f(2)}).$$
        Angenommen, es gäbe ein solches injektives $f$. Dann folgte $2^{f(3)} =
        3^{f(2)}$. Für positive Exponenten wären die beiden Zahlen teilerfremd
        und damit schon gar nicht gleich. Damit muss $f(3) = 0 = f(2)$ gelten.
        Damit ist aber $f$ wieder nicht injektiv. Wie man es dreht und wendet,
        $f$ wird nicht injektiv.

## Aufgabe 2

(a) Für $n \in \N$ gilt
    $$\abs{\frac{1}{n^s}} \leqslant \frac{1}{n},$$
    und da $\left(\frac{1}{n}\colon n \in \N\right)$ eine Nullfolge ist, ist
    auch $a$ eine Nullfolge.

(b) Zunächst betrachten wir die Folge
    $\left(\frac{x}{\sqrt{n}}\colon n \in\N\right)$. Dies ist eine Nullfolge,
    denn für $\eps > 0$ wähle $n_0 = \lceil\left(\frac{x}{\eps}\right)^2\rceil +
    1$ und nun gilt mit $n \geqslant n_0$:
    $$\left(\frac{x}{\eps}\right)^2 < n \iff \frac{x}{\eps} < \sqrt{n} \iff
    \abs{\frac{x}{\sqrt{n}}} < \eps.$$

    Wegen
    $$\abs{b_n} = \sqrt{x+n} - \sqrt{n} = \frac{(\sqrt{x+n} -
    \sqrt{n})(\sqrt{x+n} + \sqrt{n})}{\sqrt{x+n} + \sqrt{n}} =
    \frac{x + n - n}{\sqrt{x+n} + \sqrt{n}}
    = \frac{x}{\sqrt{x+n}+\sqrt{n}} < \frac{x}{\sqrt{n}}$$
    für $n \in \N$ ist auch $b$ eine Nullfolge.

## Aufgabe 3

(a) Gilt $a = 0$, so gibt es für jedes $\eps > 0$ ein $n_0 \in \N$, sodass
    $$\abs{a_n} < \eps^2,\qquad n \geqslant n_0.$$
    Es folgt
    $$\abs{\sqrt{a_n} - 0} = \abs{\sqrt{a_n}} < \varepsilon,\qquad n \geqslant
    n_0.$$
    Dies zeigt $\lim_{n\to\infty} \sqrt{a_n} = 0 = \sqrt{a}$.

    Sei nun $a \ne 0$. Es sei $\eps > 0$; dann gibt es ein $n_0 \in \N$, sodass
    $$\abs{a_n - a} < \eps^2,\qquad n \geqslant n_0.$$
    Sei $n \geqslant n_0$. Ist $a_n = a$, so gilt sofort
    $$\abs{a_n - a} = 0 < \eps.$$
    Andernfalls gilt
    $$\abs{\sqrt{a_n} - \sqrt{a}} = \abs{\frac{(\sqrt{a_n} -
    \sqrt{a})(\sqrt{a_n} + \sqrt{a})}{\sqrt{a_n} + \sqrt{a}}} = \frac{\abs{a_n
    - a}}{\sqrt{a_n} + \sqrt{a}} < \frac{\abs{a_n - a}}{\sqrt{a}} <
    \frac{\eps}{\sqrt{a}}.$$
    Dies zeigt $\lim_{n\to\infty} \sqrt{a_n} = \sqrt{a}$.

(b) Sei $b = \max\set{a_1, \dots, a_m}$. Dann gilt für $n \in \N$:
    $$\sqrt[n]{b^n} \leqslant \sqrt[n]{a_1^n + \cdots + a_m^n} \leqslant
    \sqrt[n]{mb^n}$$
    sowie
    $$b = \lim_{n\to\infty} \sqrt[n]{b^n} = \left(\lim_{n\to\infty}
    \sqrt[n]{m}\right)\left(\lim_{n\to\infty} \sqrt[n]{b^n}\right) =
    \lim_{n\to\infty} \sqrt[n]{mb^n}.$$

    Mit dem Sandwich-Lemma folgt nun
    $$\lim_{n\to\infty} \sqrt[n]{a_1^n + \cdots + a_m^n} = b = \max\set{a_1,
    \dots, a_m}.$$

## Aufgabe 4

(a) Da $a' = \left(\frac{1}{n}\right)_{n \in \N}$ eine Nullfolge ist, gibt es
    für $\eps > 0$ ein $n_0 \in \N$ mit
    $$\abs{\frac{1}{n}} < \eps^3,\qquad n \geqslant n_0.$$
    Nun folgt
    $$\abs{\frac{1}{\sqrt[3]{n}}} < \eps,\qquad n \geqslant n_0.$$
    Die Folge ist also eine Nullfolge.

(b) Angenommen, es gäbe eine Schranke $M \in \R$. Wähle nun $n = \max\set{1,
    \lceil M\rceil}$. Dann gilt:
    $$\abs{b_n} = \abs{(-1)^n \frac{n^2}{n+1}} = \frac{n^2}{n+1} = n -
    \frac{n}{n+1} > n \geqslant M.$$
    $M$ ist also gar keine Schranke, und $b$ somit unbegrenzt, also divergent.

(c) Es lässt sich ausrechnen, dass $c_3 = 0$ und $c_7 = c_8 = 1$ gilt. Die Folge
    ist also periodisch, da nach sechs Iterationen wieder die Startbedingung
    auftritt. Somit gilt $c_n = 0$ falls $n \equiv 3 \pmod{6}$ und $c_n = 1$
    falls $n \equiv 1 \pmod{6}$.

    Wir setzen $\eps = \frac{1}{2}$. Nun sei $n_0 \in \N$. Dann wählen wir
    $n = 6n_0 + 1$ und $m = 6n_0 + 3$. Es gilt $n \geqslant n_0$ und $m
    \geqslant n_0$ und ferner $n \equiv 1 \pmod{6}$ sowie $m \equiv 3 \pmod{6}$.

    Nun folgt
    $$\abs{c_m - c_n} = \abs{1 - 0} = 1 > \frac{1}{2} = \eps.$$
    Die Folge ist also keine Cauchy-Folge und daher divergent.
