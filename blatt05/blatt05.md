# Analysis I -- Blatt 5

## Aufgabe 1

(a) Die Ungleichung $n \leqslant 2^n, n \in \N$, lässt sich durch Induktion
    beweisen. Für $n = 1$ ist sie offenbar erfüllt. Gilt sie nun für ein festes
    $n$, so folgt $n + 1 \leqslant 2^n + 1 \leqslant 2^n + 2^n = 2^{n+1}$.

    Die Partialsummen der gegebenen Reihe sind wegen
    $$\sum_{k=1}^n \frac{n+2^n}{3^n} \leqslant \sum_{k=1}^n \frac{2^n +
    2^n}{3^n} = \sum_{k=1}^n 2 \cdot \left(\frac{2}{3}\right)^n \leqslant 2
    \sum_{k=1}^\infty \left(\frac{2}{3}\right)^k = 2 \cdot \frac{1}{1 -
    \frac{2}{3}} = 6$$
    für alle $n \in \N$ nach oben beschränkt. Da die Folge der Partialsummen
    außerdem monoton steigt, ist sie konvergent.

(b) Sei $a_n = (-1)^n\frac{n^4}{3^{n+1}}$ für $n \in \N$. Dann gilt
    $$\abs{\frac{a_{n+1}}{a_n}} = \frac{(n+1)^4 3^{n+1}}{n^4 3^{n+2}} =
    \frac{(n+1)^4}{3n^4} = \frac{1}{3} \left(1 + \frac{1}{n}\right)^4 \leqslant
    \frac{9}{10} < 1, \qquad n \geqslant 4.$$
    Damit ist die Reihe $\Sigma a$ laut Quotientenkriterium konvergent.

(c) Die Partialsummen der gegebenen Reihe steigen monoton und sind wegen
    $$\sum_{k=1}^n \frac{2k+1}{k^2(k+1)^2} \leqslant \sum_{k=1}^n
    \frac{3k}{k^2(k+1)^2} = \sum_{k=1}^n \frac{3}{k(k+1)^2} \leqslant
    \sum_{k=1}^n \frac{3}{k^3} = 3 \cdot \sum_{k=1}^n \frac{1}{k^3} < 3 \cdot
    \sum_{k=1}^\infty \frac{1}{k^3}, \qquad n \in \N,$$
    nach oben beschränkt. Somit ist die Folge der Partialsummen konvergent.

(d) Zunächst betrachten wir den Fall $r = 1$. Für $a \geqslant 3$ gilt
    $$\begin{aligned}
    & a^2 - 2a - 1 \geqslant 0 \\
    \iff & a^2 - 2a \geqslant 1 \\
    \iff & 2a^2 - 2a \geqslant a^2 + 1 \\
    \iff & 2a(a-1) \geqslant a^2 + 1 \\
    \iff & 2 \frac{a(a-1)}{a^2 + 1} \geqslant 1 \\
    \iff & 2 \frac{a-1}{a^2 + 1} \geqslant \frac{1}{a}.
    \end{aligned}$$

    Somit haben wir für $n \geqslant 3$:
    $$\sum_{k=1}^n 2 \frac{k^2 - k}{k^3 + k} = \sum_{k=1}^n 2 \frac{k - 1}{k^2 +
    1} = \frac{1}{5} + \sum_{k=3}^n 2 \frac{k - 1}{k^2 + 1} \geqslant
    \frac{1}{5} + \sum_{k=3}^n \frac{1}{k}.$$
    Letztere Summe ist für $n \to \infty$ unbeschränkt. Somit divergiert
    $$\sum_{n=1}^\infty 2 \frac{n - 1}{n^2 + 1}, \qquad \text{und damit auch}
    \qquad \sum_{n=1}^\infty \frac{n-1}{n^2 + 1}.$$

    Für $r \geqslant 2$ steigen die Partialsummen monoton und sind wegen
    $$\begin{aligned}
    \sum_{k=1}^n \frac{k^2 - k}{(k^3 + k)^r} &= \sum_{k=1}^n \frac{k^2 - k}{k^3
    + k} \cdot \frac{1}{(k^3 + k)^{r-1}} = \sum_{k=1}^n \frac{k - 1}{k^2 + 1}
    \cdot \frac{1}{(k^3 + k)^{r-1}} \\
    &\leqslant \sum_{k=1}^n \frac{k}{k^2} \cdot \frac{1}{k^3 + k} = \sum_{k=1}^n
    \frac{k}{k^2(k^3+k)} = \sum_{k=1}^n \frac{1}{k(k^3+k)} \\
    &\leqslant \sum_{k=1}^n \frac{1}{k^4} < \sum_{k=1}^\infty \frac{1}{k^4},
    \qquad n \in \N,
    \end{aligned}$$
    nach oben beschränkt. Somit ist die Folge der Partialsummen konvergent.

## Aufgabe 2

(1) Sei $\eps > 0$. Wähle $\delta = \eps$ und erhalte unter Zuhilfenahme der
    umgekehrten Dreiecksungleichung für $x, x' \in \R$ mit $\abs{x' - x} <
    \delta$:
    $$\abs{f(x') - f(x)} = \abs{\abs{x'} - \abs{x}} \leqslant \abs{\abs{x' - x}}
    = \abs{x' - x} < \delta = \eps.$$

(2) Zunächst bemerken wir, dass für $a, b \in \R$ gilt:
    $$\frac{a + b}{(a^2 + 1)(b^2 + 1)} = \frac{a}{(a^2 + 1)\underbrace{(b^2 +
    1)}_{\geqslant 1}} + \frac{b}{\underbrace{(a^2 + 1)}_{\geqslant 1}(b^2 +
    1)} \leqslant \frac{a}{\underbrace{a^2 + 1}_{\geqslant a}} +
    \frac{b}{\underbrace{b^2 + 1}_{\geqslant b}} \leqslant 1 + 1 = 2.$$

    In diesem Wissen sei nun $x_0 \in \R$ und $\eps > 0$. Um die Stetigkeit von
    $f$ an der Stelle $x_0$ zu zeigen, wählen wir $\delta = \frac{\eps}{2}$.
    Dann erhalten wir für $x \in \R$ mit $\abs{x - x_0} < \delta$:
    $$\begin{aligned}
    \abs{f(x) - f(x_0)} &= \abs{\frac{1}{x^2 + 1} + \frac{1}{x_0^2 + 1}} =
    \abs{\frac{(x_0^2 + 1) - (x^2 + 1)}{(x_0^2 + 1)(x^2 + 1)}} =
    \abs{\frac{x_0^2 - x^2}{(x_0^2 + 1)(x^2 + 1)}} \\
    &= \abs{\frac{(x_0 - x)(x_0 + x)}{(x_0^2 + 1)(x^2 + 1)}} =
    \abs{x_0 - x}\abs{\frac{x_0 + x}{(x_0^2 + 1)(x^2 + 1)}} \leqslant
    2\abs{x_0 - x} < 2 \delta = \eps.
    \end{aligned}$$

## Aufgabe 3

Für $n \geqslant n_0$ gilt $\sqrt[n]{\abs{a_n}} \leqslant \theta$, also
$\abs{a_n} \leqslant \theta^n$. Die Partialsummen von $\Sigma \abs{a}$ sind
wegen
$$\sum_{k=0}^n \abs{a_n} = \underbrace{\sum_{k=0}^{n_0 - 1} \abs{a_n}}_{=:
s} + \sum_{k=n_0}^n \abs{a_n} \leqslant s + \sum_{k=n_0}^n \theta^k \leqslant s
+ \frac{1}{1 - \theta}$$
für alle $n \in \N$ nach oben beschränkt. Da die Folge der Partialsummen von
$\Sigma \abs{a}$ außerdem monoton steigt, ist sie konvergent. Somit konvergiert
$\Sigma a$ absolut.

(a) Es gilt
    $$\sqrt[n]{\abs{\frac{3^n}{n^n}}} = \sqrt[n]{\frac{3^n}{n^n}} = \frac{3}{n}
    \leqslant \frac{3}{4}, \qquad n \geqslant 4.$$
    Das Wurzelkriterium liefert die absolute Konvergenz der Reihe.

(b) Es gilt
    $$\sqrt[n]{\abs{\left(\frac{2n+3}{3n+2}\right)^n}} = \frac{2n+3}{3n+2}
    \leqslant \frac{7}{8}, \qquad n \geqslant 2.$$
    Das Wurzelkriterium liefert die absolute Konvergenz der Reihe.

## Aufgabe 4

(a) Sei $\sum_{n=1}^\infty b_n$ konvergent. Definiere $\lambda =
    \frac{b_1}{a_1}$ und betrachte die Reihe $\sum_{n=1}^\infty c_n$ mit $c_n
    = \lambda a_n,\;n \in \N$.

    Durch Induktion lässt sich zeigen, dass $c_n \leqslant b_n$ für alle $n \in
    \N$ gilt:

    **Induktionsanfang**: Mit $n = 1$ haben wir
    $$c_n = c_1 = \lambda a_1 = \frac{b_1}{a_1} a_1 = b_1 \leqslant b_1 = b_n.$$

    **Induktionsvoraussetzung**: Für ein festes $n$ gelte $c_n \leqslant b_n$.

    **Induktionsschritt**: Es gilt
    $$\frac{a_{n+1}}{a_n} \leqslant \frac{b_{n+1}}{b_n}, \qquad \text{also}
    \qquad a_{n+1} \leqslant b_{n+1} \frac{a_n}{b_n}.$$
    Wegen der Induktionsvoraussetzung gilt $\frac{c_n}{b_n} \leqslant 1$, und es
    folgt
    $$c_{n+1} = \lambda a_{n+1} \leqslant \lambda b_{n+1} \frac{a_n}{b_n}
    = b_{n+1} \frac{c_n}{b_n} \leqslant b_{n+1}.$$

    Für die Partialsummen von $\sum_{n=1}^\infty c_n$ gilt also
    $$\sum_{k=1}^n c_k \leqslant \sum_{k=1}^n b_k \leqslant \sum_{k=1}^\infty
    b_k, \qquad n \in \N,$$
    und letztere Reihe konvergiert. Damit sind auch die Partialsummen von
    $\sum_{n=1}^\infty a_n$ beschränkt, denn es gilt
    $$\sum_{k=1}^n a_k = \sum_{k=1}^n \frac{c_k}{\lambda} = \frac{1}{\lambda}
    \sum_{k=1}^n c_k \leqslant \frac{1}{\lambda} \sum_{k=1}^\infty b_k, \qquad n
    \in \N.$$
    Da sie außerdem monoton steigen, ist $\sum_{n=1}^\infty a_n$ konvergent.

(b) Dies ist lediglich die Kontraposition von (a).
