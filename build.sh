#!/usr/bin/env bash
[ -d "build" ] || mkdir build
pandoc -H include/preamble.tex -B include/header.tex blatt$1/*.md -o build/blatt$1.pdf
