# Analysis I -- Blatt 8

## Aufgabe 1

$f'$ bezeichne jeweils die Ableitungsfunktion von $f$.

(a) $f'(x) = nx^{n-1}a^x + x^na^x\log(a)$.
(b) $f'(x) = -\frac{\sin(x)\cos(x)}{1 + \cos^2(x)}$.
(c) $f'(x) = \frac{\sqrt{\exp(\sin(\sqrt{x}))} \cdot
    \cos(\sqrt{x})}{4\sqrt{x}}$.
(d) Es gilt $f(x) = \exp(p \cdot \log(x))$.
    Somit folgt $f'(x) = \exp(p \cdot \log(x)) \cdot \frac{p}{x} =
    px^{p-1}$.
(e) $f'(x) = \frac{\left(1 - 2 \sin^2\left(\frac{x}{2}\right)\right)
    \tan(x)}{\cos^(x) \sqrt{1 + \tan^2(x)}} - 2 \sin\left(\frac{x}{2}\right)
    \cos\left(\frac{x}{2}\right) \sqrt{1 + \tan^2(x)}$.

## Aufgabe 2

Es gilt $f(x) = -x \sin(x)$ für $x \in \R_-$ und $f(x) = x \sin(x)$ für $x \in
\R_+$. Die Differenzierbarkeit auf $\R \nozero$ ergibt sich also daraus, dass
$f$ aus differenzierbaren Funktionen zusammengesetzt ist. Die Funktion $f$ ist
ferner differenzierbar an der Stelle 0, da gilt
$$\lim_{h \to 0} \frac{f(h) - f(0)}{h} = \lim_{h \to 0} \frac{\abs{h}
\sin(h)}{h} = 0 = f'(x),$$
wobei $f'$ die Ableitung von $f$ sei. Es gilt
$$f'(x) = \begin{cases}
\sin(x) +  x\cos(x) & \text{für } x > 0, \\
0 & \text{für } x = 0, \\
-\sin(x) - x \cos(x) & \text{für } x < 0.
\end{cases}$$

Für Stetigkeit und Differenzierbarkeit ist wieder nur die Stelle 0 von
Interesse. Sei $(x_n)$ eine beliebige Nullfolge. Dann gilt
$$\lim_{n \to \infty} f'(x_n) \leqslant \lim_{n \to \infty}
(\underbrace{\sin(x_n)}_{\to 0} + \underbrace{x_n \cos(x_n)}_{\to 0}) = 0$$
und
$$\lim_{n \to \infty} f'(x_n) \geqslant \lim_{n \to \infty}
(-\underbrace{\sin(x_n)}_{\to 0} - \underbrace{x_n \cos(x_n)}_{\to 0}) = 0,$$
also $\lim_{n \to \infty} f'(x_n) = 0 = f'\left(\lim_{n \to \infty} x_n\right)$.

Mit demselben Argument wie bei Blatt 6, Aufgabe 1, divergiert $(x_n)$ mit
$x_n = n \sin\left(\frac{1}{n}\right) + \cos\left(\frac{1}{n}\right),\;n \in
\N$. Betrachtet man nun die harmonische Nullfolge $(x_n)$, so existiert der
folgende Grenzwert nicht:
$$\lim_{n \to \infty} \frac{f'(x_n) - f'(0)}{x_n} = \lim_{n \to \infty} n
\left(\sin\left(\frac{1}{n}\right) + \frac{1}{n}
\cos\left(\frac{1}{n}\right)\right) =
\lim_{n \to \infty} n \sin\left(\frac{1}{n}\right) + \cos\left(\frac{1}{n}\right).$$
Daher ist $f'$ an der Stelle 0 zwar stetig, aber nicht differenzierbar.

## Aufgabe 3

(a) Es gilt
    $$\begin{aligned}
    f^{(1)}(x) &= -\ee^{-x}\sin(x) + \ee^{-x}\cos(x) \\
    f^{(2)}(x) &= -2 \ee^{-x} \cos(x) \\
    f^{(3)}(x) &= 2 (\ee^{-x} \cos(x) + \ee^{-x} \sin(x)) \\
    f^{(4)}(x) &= -4 \ee^{-x} \sin(x) = -4 f(x).
    \end{aligned}$$

(b) Die Funktion $g$ ist auf $\R \nozero$ differenzierbar, da aus
    differenzierbaren Funktionen zusammengesetzt. Für die Differenzierbarkeit an
    der Stelle 0 haben wir für alle $h \in \R \nozero$:
    $$\abs{h \sin\left(\frac{1}{h}\right)} \leqslant \abs{h}, \qquad \text{also}
    \qquad -\lim_{h \to 0} \abs{h} \leqslant \lim_{h \to 0} h
    \sin\left(\frac{1}{h}\right) \leqslant \lim_{h \to 0} \abs{h}.$$
    Somit existiert die Ableitung $g'$ an der Stelle 0:
    $$g'(x) = \lim_{h \to 0} \frac{g(h) - g(0)}{h} = \lim_{h \to 0} \frac{h^2
    \sin\left(\frac{1}{h}\right)}{h} = \lim_{h \to 0} h
    \sin\left(\frac{1}{h}\right) = 0.$$

    Funktionsgraph:

    \begin{center}
    \begin{tikzpicture}[scale=5]
        \draw[black!35!white,step=0.25] (-0.99, -0.99) grid (0.99, 0.99);

        % axes
        \draw[thick,->] (-1, 0) -- (1, 0) node[anchor=west] {$x$};
        \draw[thick,->] (0, -1) -- (0, 1) node[anchor=south] {$g(x)$};

        \draw (0.5, 0) node[anchor=north] {$0.5$};
        \draw (-0.5, 0) node[anchor=north] {$-0.5$};
        \draw (0, 0.5) node[anchor=west] {$0.5$};
        \draw (0, -0.5) node[anchor=west] {$-0.5$};

        \draw[thick,domain=-0.9:-0.01,smooth,samples=100,blue] plot
        ({\x},{\x*\x*sin(deg(1/\x)});
        \draw[thick,domain=0.01:0.9,smooth,samples=100,blue] plot
        ({\x},{\x*\x*sin(deg(1/\x))});
    \end{tikzpicture}
    \end{center}

## Aufgabe 4

Betrachte $g \colon [a, b] \to [a, b]$ mit $g(x) = f(x) - x$. Dann gilt $g(a) =
f(a) - a > 0$ und $g(b) = f(b) - b < 0$ und laut Nullstellensatz besitzt $g$
eine Nullstelle $x_0 \in [a, b]$. Nun folgt
$$f(x_0) = f(x_0) - x_0 + x_0 = g(x_0) + x_0 = x_0.$$
