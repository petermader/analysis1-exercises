# Analysis I -- Blatt 7

## Aufgabe 1

(a) Die Funktion $\sinh$ ist als Zusammensetzung von stetigen Funktionen
    wiederum stetig. Sie ist außerdem streng monoton steigend, denn für $x, y
    \in \R$ mit $x > y$ gilt
    $$\sinh(x) - \sinh(y) = \frac{1}{2}(\ee^x - \ee^{-x}) - \frac{1}{2}(\ee^y -
    \ee^{-y}) = \frac{1}{2}(\underbrace{\ee^x - \ee^y}_{> 0}) +
    \frac{1}{2}(\underbrace{\ee^{-y} - \ee^{-x}}_{> 0}) > 0,$$
    also $\sinh(x) > \sinh(y)$. Laut Satz 5.1.2 besitzt $\sinh$ somit eine
    Umkehrfunktion. Sie ist eindeutig bestimmt.

    Betrachte die Funktion $f \colon \R \to \R$ mit $f(x) = \log(x + \sqrt{x^2 +
    1})$. Für alle $x \in \R$ gilt $\cosh(x) > 0$. Unter Anwendung von (b)
    ergibt sich:
    $$\begin{aligned}
    f(\sinh(x)) &= \log\left(\sinh(x) + \sqrt{\sinh^2(x) + 1}\right) =
    \log\left(\sinh(x) + \sqrt{\cosh^2(x)}\right) = \log\left(\sinh(x) +
    \cosh(x)\right) \\
    &= \log\left(\frac{1}{2}(\ee^x - \ee^{-x}) + \frac{1}{2}(\ee^x +
    \ee^{-x})\right) = \log(\ee^x) = x = \mathrm{id}_\R.
    \end{aligned}$$
    Somit haben wir $f \circ \sinh = \mathrm{id}_\R$. $f$ ist also die
    Umkehrfunktion zu $\sinh$.

(b) Für alle $x \in \R$ gilt:
    $$\begin{aligned}
    \cosh^2x - \sinh^2x &= \frac{1}{4}(\ee^x + \ee^{-x})^2 - \frac{1}{4}(\ee^x
    - \ee^{-x})^2 \\
    &= \frac{1}{4}(\ee^{2x} + 2 + \ee^{-2x}) - \frac{1}{4}(\ee^{2x} - 2 +
    \ee^{-2x}) = \frac{4}{4} = 1.
    \end{aligned}$$

(c) Für alle $x, y \in \R$ gilt:
    $$\begin{aligned}
    \sinh(x)\cosh(y) + \sinh(y)\cosh(x) &= \frac{1}{4}(\ee^x - \ee^{-x})(\ee^y +
    \ee^{-y}) + \frac{1}{4}(\ee^y - \ee^{-y})(\ee^x + \ee^{-x}) \\
    &= \frac{\ee^{x+y} + \ee^{x-y} - \ee^{y-x} - \ee^{-x-y}}{4} +
    \frac{\ee^{x+y} + \ee^{y-x} - \ee^{x-y} - \ee^{-x-y}}{4} \\
    &= \frac{2\ee^{x+y} - 2\ee^{-x-y}}{4} = \frac{1}{2}(\ee^{x+y} - \ee^{-x-y})
    \\ &= \sinh(x + y)
    \end{aligned}$$
    sowie
    $$\begin{aligned}
    \cosh(x)\cosh(y) + \sinh(x)\sinh(y) &= \frac{1}{4}(\ee^x + \ee^{-x})(\ee^y +
    \ee^{-y}) + \frac{1}{4}(\ee^x - \ee^{-x})(\ee^y - \ee^{-y}) \\
    &= \frac{\ee^{x+y} + \ee^{x-y} + \ee^{y-x} + \ee^{-x-y}}{4} +
    \frac{\ee^{x+y} - \ee^{x-y} - \ee^{y-x} + \ee^{-x-y}}{4} \\
    &= \frac{2\ee^{x+y} + 2\ee^{-x-y}}{4} = \frac{1}{2}(\ee^{x+y} + \ee^{-x-y})
    \\ &= \cosh(x + y).
    \end{aligned}$$

## Aufgabe 2

Für alle $n \in \N$ gilt:
$$\begin{aligned}
\sin\left(\left(n - \frac{1}{2}\right)x\right) +
2\sin\left(\frac{x}{2}\right)\cos(nx) &= \cos(nx)\sin\left(-\frac{x}{2}\right) +
\sin(nx)\cos\left(\frac{x}{2}\right) + 2\sin\left(\frac{x}{2}\right)\cos(nx) \\
&= -\cos(nx)\sin\left(\frac{x}{2}\right) + \sin(nx)\cos\left(\frac{x}{2}\right)
+ 2\sin\left(\frac{x}{2}\right)\cos(nx) \\
&= \sin(nx)\cos\left(\frac{x}{2}\right) + \sin\left(\frac{x}{2}\right)\cos(nx) =
\sin\left(nx + \frac{x}{2}\right).
\end{aligned}$$

Induktionsbeweis nach $n$:

**Induktionsanfang**: mit $n = 1$ gilt:
$$\begin{aligned}
\frac{1}{2} + \sum_{k=1}^n \cos(kx) &= \frac{1}{2} + \cos(x) =
\frac{\sin\left(\frac{x}{2}\right) + 2\sin\left(\frac{x}{2}\right)\cos(x)}{2
\sin\left(\frac{x}{2}\right)} \\
&= \frac{\sin\left(\left(n - \frac{1}{2}\right)x\right) +
2\sin\left(\frac{x}{2}\right) \cos(nx)}{2 \sin\left(\frac{x}{2}\right)} =
\frac{\sin\left(nx + \frac{x}{2}\right)}{2\sin\left(\frac{x}{2}\right)}.
\end{aligned}$$

**Induktionsvoraussetzung**: Sei $n > 1$ fest. Die Behauptung gelte für $n - 1$.

**Induktionsschritt**: Nun folgt mit der Induktionsvoraussetzung:
$$\begin{aligned}
\frac{1}{2} + \sum_{k=1}^n \cos(kx) &= \frac{1}{2} + \sum_{k=1}^{n-1} \cos(kx)
+ \cos(nx) \stackrel{\text{IH}}{=}
\frac{\sin\left(\left(n-\frac{1}{2}\right)x\right)}{2
\sin\left(\frac{x}{2}\right)} + \cos(nx) \\
&= \frac{\sin\left(\left(n-\frac{1}{2}\right)x\right) +
2\sin\left(\frac{x}{2}\right)\cos(nx)}{2
\sin\left(\frac{x}{2}\right)} \\
&= \frac{\sin\left(\left(n+\frac{1}{2}\right)x\right)}{2
\sin\left(\frac{x}{2}\right)}.
\end{aligned}$$

## Aufgabe 3

(a) Es gilt
    $$\begin{aligned}
    (A(a, b))^2 &= \frac{(a+b)^2}{4} = \frac{a^2 + 2ab + b^2}{4} = \frac{a^2 -
    2ab + b^2 + 4ab}{4} \\
    &= \frac{a^2-2ab+b^2}{4} + ab = \frac{(a-b)^2}{4} + ab
    \geqslant ab = (G(a, b))^2.
    \end{aligned}$$
    Also folgt $A(a, b) \geqslant G(a, b)$, und damit $\frac{G(a, b)}{A(a, b)}
    \leqslant 1$.
    Dies liefert
    $$H(a, b) = \frac{2}{\frac{1}{a} + \frac{1}{b}} = \frac{2}{\frac{a+b}{ab}} =
    \frac{2ab}{a+b} = G(a, b) \cdot \frac{G(a, b)}{A(a, b)} \leqslant G(a, b).$$

(b) Die Ungleichung $A(a, b) \geqslant G(a, b)$ für $a, b \in \R$ in (a) lässt
    sich modifizieren zu $A(a, b) > G(a, b)$, falls man $a \ne b$ voraussetzt.
    Dann ergibt sich
    $$\ee^{\frac{x+y}{2}} = \sqrt{\ee^{x+y}} = \sqrt{\ee^x\ee^y} = G(\ee^x,
    \ee^y) < A(\ee^x, \ee^y) = \frac{\ee^x + \ee^y}{2}.$$
    Bildet man also zuerst das arithmetische Mittel von $x$ und $y$ und
    betrachtet dessen Exponential, so erhält man einen geringeren Wert, als wenn
    man zuerst das Exponential berechnet und anschließend das arithmetische
    Mittel bildet. Ist $x < y$, so steigt die Exponentialfunktion somit im
    Intervall $\left[x, \frac{x+y}{2}\right]$ langsamer als im Intervall
    $\left[\frac{x+y}{2}, y\right]$, ist also linksgekrümmt.

## Aufgabe 4

(a) Sei $\eps > 0$. Da die Folge $\left(\frac{1}{n}\right)_{n \in \N}$ gegen 0
    konvergiert, gibt es ein $n_0$, sodass
    $$\abs{\frac{1}{n}} < \frac{\eps}{4}, \qquad n \geqslant n_0.$$
    Es folgt
    $$\abs{a_n} = \abs{\frac{4\ii^n}{n}} = 4 \abs{\ii^n} \abs{\frac{1}{n}} = 4
    \abs{\frac{1}{n}} < \eps.$$

    Wieder sei $\eps > 0$. Es gilt $d = \frac{\sqrt{5}}{4} < 1$. Daher
    konvergiert $\left(d^n\right)_{n\in\N}$ gegen 0 und es gibt ein $n_0$,
    sodass
    $$\abs{d^n} < \eps, \qquad n \geqslant n_0.$$
    Es folgt
    $$\abs{b_n} = \abs{\left(\frac{1}{4} + \frac{1}{2}\ii\right)^n} =
    \abs{\frac{1}{4} + \frac{1}{2}\ii}^n = \left(\frac{\sqrt{5}}{4}\right)^n =
    d^n < \eps.$$

(b) Die Teilfolgen $(c_{4n}), (c_{4n+1}), (c_{4n+2})$ und $(c_{4n+3})$
    konvergieren (mit demselben Argument wie oben) und es gilt
    $$\begin{aligned}
    & \lim_{n\to\infty} c_{4n} = \lim_{n\to\infty} \ii^{4n} +
    \left(-\frac{1}{2}\right)^{4n} = 1, \\
    & \lim_{n\to\infty} c_{4n+1} = \lim_{n\to\infty} \ii^{4n+1} +
    \left(-\frac{1}{2}\right)^{4n+1} = \ii, \\
    & \lim_{n\to\infty} c_{4n+2} = \lim_{n\to\infty} \ii^{4n+2} +
    \left(-\frac{1}{2}\right)^{4n+2} = -1, \\
    & \lim_{n\to\infty} c_{4n+3} = \lim_{n\to\infty} \ii^{4n+3} +
    \left(-\frac{1}{2}\right)^{4n+3} = -\ii.
    \end{aligned}$$
    Diese Teilfolgen zusammen ergeben die Folge $(c_n)$. Da sie alle
    konvergieren, kann es keine anderen Häufungspunkte als $1, \ii, -1, -\ii$
    geben.
