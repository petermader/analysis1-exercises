# Analysis I -- Blatt 2

## Aufgabe 1

Wegen $\abs{a} = \abs{-a}$ für $a \in \K$ folgt für $b, c \in K$:
$$\abs{b - c} = \abs{-(c - b)} = \abs{c - b}. \tag{$*$}$$

Ferner gilt wegen der Dreiecksungleichung für $a, b \in \K$:
$$a + b \leqslant \abs{a + b} \leqslant \abs{a} + \abs{b}. \tag{$**$}$$

(a) Wir unterscheiden vier Fälle und wenden jeweils die Dreiecksungleichung an:
    * $x - x' \geqslant 0, y - y' \geqslant 0$: es gilt mit $(*)$:
      $$\begin{aligned}
      \abs{\abs{x - x'} - \abs{y - y'}} &= \abs{x - x' - y + y'} \\
      &= \abs{(x - y) + (y' - x')} \\
      &\leqslant \abs{x - y} + \abs{y' - x'} \\
      &= \abs{x - y} + \abs{x' - y'}.
      \end{aligned}$$
    * $x - x' \geqslant 0, y - y' < 0$: es gilt mit $(*)$ und $(**)$:
      $$\begin{aligned}
      \abs{\abs{x - x'} - \abs{y - y'}} &= \abs{x - x' + y - y'} \\
      &= \abs{(x - x') + (y - y')} \\
      &\leqslant \abs{x - x'} + \abs{y - y'} \\
      &= x - x' - y + y' \\
      &= (x - y) + (y' - x') \\
      &\leqslant \abs{x - y} + \abs{y' - x'} \\
      &= \abs{x - y} + \abs{x' - y'}.
      \end{aligned}$$
    * $x - x' < 0, y - y' \geqslant 0$: es gilt mit $(*)$ und $(**)$:
      $$\begin{aligned}
      \abs{\abs{x - x'} - \abs{y - y'}} &= \abs{-x + x' - y + y'} \\
      &= \abs{-(x - x' + y + y')} \\
      &= \abs{x - x' + y - y'} \\
      &= \abs{(x - x') + (y - y')} \\
      &\leqslant \abs{x - x'} + \abs{y - y'} \\
      &= x - x' - y + y' \\
      &= (x - y) + (y' - x') \\
      &\leqslant \abs{x - y} + \abs{y' - x'} \\
      &= \abs{x - y} + \abs{x' - y'}.
      \end{aligned}$$
    * $x - x' < 0, y - y' < 0$: es gilt mit $(*)$:
      $$\begin{aligned}
      \abs{\abs{x - x'} - \abs{y - y'}} &= \abs{x' - x - y' + y} \\
      &= \abs{(y - x) + (x' - y')} \\
      &\leqslant \abs{y - x} + \abs{x' - y'} \\
      = \abs{x - y} + \abs{x' - y'}.
      \end{aligned}$$

(b) Wir unterscheiden zwei Fälle und wenden jeweils die Dreiecksungleichung an:
    * $\abs{x} - \abs{y} \geqslant 0$: es gilt
      $$\abs{x} = \abs{x + y - y} = \abs{(x + y) + (-y)} \leqslant \abs{x + y} +
      \abs{-y} = \abs{x + y} + \abs{y}$$
      und beidseitige Subtraktion von $\abs{y}$ liefert
      $$\abs{x} - \abs{y} \leqslant \abs{x + y},$$
      also
      $$\abs{\abs{x} - \abs{y}} = \abs{x} - \abs{y} \leqslant \abs{x + y}.$$
    * $\abs{x} - \abs{y} < 0$: analog gilt
      $$\abs{y} \leqslant \abs{y + x} + \abs{x},$$
      also
      $$\abs{y} - \abs{x} \leqslant \abs{x + y}$$
      und daher
      $$\abs{\abs{x} - \abs{y}} = \abs{y} - \abs{x} \leqslant \abs{x + y}.$$

(c) Unter Anwendung der Dreiecksungleichung gilt
    $$\abs{y} + \abs{x - y} \geqslant \abs{y + x - y} = \abs{x}.$$
    Beidseitige Subtraktion von $\abs{y}$ liefert
    $$\abs{x - y} \geqslant \abs{x} - \abs{y}.$$

## Aufgabe 2

(a) $\Pow(M) = \set{\varnothing, \set{1}, \set{2}, \set{3}, \set{1, 2}, \set{1,
    3}, \set{2, 3}, \set{1, 2, 3}}.$

(b) **Reflexivität**: offenbar gilt $A \subseteq A$ für $A \in \Pow(M)$.

    **Transitivität**: seien $A, B, C \in \Pow(M)$ mit $A \subseteq B$ und $B
    \subseteq C$. Nun sei $x \in A$. Wegen $A \subseteq B$ gilt $x \in B$. Wegen
    $B \subseteq C$ gilt $x \in C$. Also folgt $A \subseteq C$.

    **Antisymmetrie**: seien $A, B \in \Pow(M)$ mit $A \subseteq B$ und $B
    \subseteq A$. Für alle $x \in A$ gilt $x \in B$, für alle $x \in B$ gilt $x
    \in A$, also $A = B$.

    Die Ordnung ist echt partiell, da für $A = \set{1}$ und $B = \set{2}$ weder
    $A \subseteq B$ noch $B \subseteq A$ gilt.

(c) Wenn die Ordnung total sein soll, müssen zwei beliebige $A, B \in \Pow(M)$
    vergleichbar sein; dazu muss eine Menge eine Teilmenge der anderen sein.
    Dies ist genau dann der Fall, wenn die Grundmenge $M$ weniger als zwei
    Elemente enthält.

## Aufgabe 3

(a) Sei $A = \set{1}, B = \set{1, 2}$. Die Abbildung
    $$\begin{aligned}f \colon A &\to B \\
    1 &\mapsto 1\end{aligned}$$
    ist offenbar injektiv, aber nicht surjektiv, da es kein
    $a \in A$ gibt mit $f(a) = 2$.
(b) Sei $A = \set{1, 2}, B = \set{1}$. Die Abbildung
    $$\begin{aligned}f \colon A &\to B \\
    1 &\mapsto 1,\\
    2 &\mapsto 1\end{aligned}$$
    ist offenbar surjektiv, aber nicht injektiv, da $1 \ne 2$, aber $g(1) =
    g(2)$.
(c) Die Abbildung ist nicht injektiv, da $0 \ne 2$, aber $h_1(0) = 0 = h_1(2)$,
    und damit auch nicht bijektiv.

    Sie ist aber surjektiv.
(d) Die Abbildung ist nicht injektiv, da $0 \ne 2\pi$, aber $h_2(0) = 1 =
    h_2(2\pi)$. Die Abbildung ist nicht surjektiv, da es kein $x \in \R$ gibt
    mit $h_2(x) = 2$, und daher auch nicht bijektiv.

## Aufgabe 4

(a) Es sei $Z = f(X) \subseteq Y$. Ferner sei $h \colon X \to f(X)$ gegeben
    durch $h(x) = f(x)$ für $x \in X$ und $g \colon f(X) \xhookrightarrow{} Y$
    gegeben durch $g(y) = y$ für $y \in f(x)$.

    Nun ist $h$ per Konstruktion surjektiv und $g$ als Inklusionsfunktion von
    $f(X)$ in $Y$ injektiv. Ferner gilt $(g \circ h)(x) = g(h(x)) = g(f(x)) =
    f(x)$ für $x \in X$, also $f = g \circ h$.

(b) Wir setzen $Z = X \times Y$.

    Es sei $a \in f(X)$ beliebig, aber fest. Dann definieren wir $h \colon X \to
    Z$ durch $h(x) = (x, a)$ für $x \in X$ und $g \colon Z \to Y$ durch
    $$g(x, y) = \begin{cases}
    f(x) & \text{falls $y = a$}, \\
    y & \text{sonst.}
    \end{cases}$$

    Die Abbildung $h$ ist injektiv, denn für $x, x' \in X$ mit $x \ne x'$ gilt
    $$h(x) = (x, a) \ne (x', a) = h(x').$$

    Um zu zeigen, dass $g$ surjektiv ist, sei $y \in Y$. Dann tritt einer von
    zwei Fällen ein:

    * $y = a \in f(x)$. Dann gibt es ein $x \in X$ mit $f(x) = y$, und wir haben
      $$g(f(x)) = g(x, a) = f(x) = y.$$
    * $y \ne a$. Wähle ein $x \in X$. Dann gilt $g(x, y) = y$.

    Ferner gilt für $x \in X$:
    $$(g \circ h)(x) = g(h(x)) = g(x, a) = f(x),$$
    also $f = g \circ h$.
