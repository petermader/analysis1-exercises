# Analysis I, Blatt 1

## Aufgabe 1

\paragraph{Reflexivität:} sei $x \in X$. Die folgenden sich gegenseitig
ausschließenden Fälle ergeben sich:

* $x \in A$: wegen $A \times A \subseteq R$ gilt $(x, x) \in R$.
* $x \in B$: analog.
* $x \in C$: analog.

\paragraph{Symmetrie:} sei $(x, y) \in R$. Die Mengen $A \times A, B \times B$
und $C \times C$ sind disjunkt, wieder ergeben sich die folgenden Fälle:

* $(x, y) \in A \times A$: dann gilt $x \in A$, $y \in A$ und es folgt $(y, x)
  \in A$.
* $(x, y) \in B \times B$: analog.
* $(x, y) \in C \times C$: analog.

\paragraph{Transitivität:} seien $(x, y), (y, z) \in R$. Einmal mehr ergeben
sich die folgenden Fälle:

* $(x, y) \in A \times A$: dann gilt $x \in A$ und insbesondere $y \in A$,
  woraus sich $z \in A$ ergibt. Es folgt $(x, z) \in A$.
* $(x, y) \in B \times B$: analog.
* $(x, y) \in C \times C$: analog.

## Aufgabe 2

(a) Die Kommutativität von Addition und Multiplikation lässt sich an der
    Symmetrie der Tabellen ablesen. Ferner besitzt in der additiven Gruppe
    $(\F_3, +)$ sowie in der multiplikativen Gruppe $(\F_3 \setminus \set{0},
    \cdot)$ jedes Element ein Inverses, da jedes Element in jeder Zeile und
    Spalte genau einmal vorkommt.

    Assoziativität und Distributivität sind nicht so einfach zu sehen.  Wir
    bemerken dazu, dass die Operationen genau dieselben wie bei der Arithmetik
    modulo 3. Somit ist $\F_3$ isomorph zu $\Z/3\Z$, und das ist ein Körper.

(b) Ein Körper muss nullteilerfrei sein. Hier gilt aber $2 \cdot 2 = 0$. Somit
    ist $\F_4$ mit den angegebenen Operationen kein Körper.

## Aufgabe 3

(a) Sei $m \in M$ ein Analysis-Student. Dann ist $m$ im selben Land wie $m$
    geboren, also $m \equiv m$, und die Relation ist reflexiv.

    Seien $m_1, m_2 \in M$ mit $m_1 \equiv m_2$. $m_1$ ist also im selben Land
    wie $m_2$ geboren, und umgekehrt ist $m_2$ im selben Land wie $m_1$ geboren.
    Es folgt $m_2 \equiv m_1$, und die Relation ist symmetrisch.

    Seien $m_1, m_2, m_3 \in M$ mit $m_1 \equiv m_2$ sowie $m_2 \equiv m_3$.
    Dann ist $m_1$ im selben Land geboren wie $m_2$, welchselbiger Student sich
    sein Geburtsland mit $m_3$ teilt. Daher entspringen $m_1$ und $m_3$
    derselben Nation, und wir haben $m_1 \equiv m_3$. Also ist die Relation
    transitiv, was sie insgesamt einer Äquivalenzrelation macht.

(b) Es sei $n \in \N$. Dann lässt $n$ bei der Division durch 5 offenbar
    denselben Rest wie $n$, also $n \equiv n$, und die Relation ist reflexiv.

    Nun seien $n, m \in \N$ mit $n \equiv m$. Dann lässt $m$ bei der Division
    durch 5 denselben Rest wie $n$, also $m \equiv n$, und die Relation ist
    symmetrisch.

    Schließlich seien $n, m, o \in \N$ mit $n \equiv m$ sowie $m \equiv o$. Es
    gibt also $x_1, x_2 \in \Z$ mit $n = 5x_1 + m$ und $m = 5x_2 + o$.
    Nun gilt $n = 5(x_1 + x_2) + o$, und $n$ und $o$ lassen bei der Division
    durch 5 denselben Rest. Also folgt $n \equiv o$, und die Transitivität
    zeigt, dass die Relation eine Äquivalenzrelation ist.

(c) Hanna mag Peter. Peter mag Daniel. Hanna kennt Daniel nicht, ihr fällt es
    deshalb auch schwer, ihn zu mögen. Daher ist die Relation nicht transitiv
    und daher keine Äquivalenzrelation.

## Aufgabe 4

Sämtliche Körpereigenschaften vererben sich von $\Q$ auf $F = \Q(\sqrt{5})$.
Somit ist auch $F$ ein Körper. Interessant ist vielleicht noch das inverse
Element der Multiplikation.

Dazu sei $x = a + b\sqrt{5} \in F$, wobei $a \ne 0$ oder $b \ne 0$ gelte. Dann
gilt $a^2 + 5b^2 \ne 0$ und das multiplikative Inverse zu $x$ ist gegeben durch
$$x^{-1} = \frac{a}{a^2 + 5b^2} - \frac{b}{a^2 + 5b^2}\sqrt{5} \in F,$$
denn
$$xx^{-1} = (a + b\sqrt{5})\left(\frac{a}{a^2 + 5b^2} - \frac{b}{a^2 +
5b^2}\sqrt{5}\right) = \frac{a^2}{a^2 + 5b^2} + \frac{-ab + ba}{a^2 +
5b^2}\sqrt{5} + \frac{5b^2}{a^2 + 5b^2} = \frac{a^2 + 5b^2}{a^2 + 5b^2} = 1.$$
