# Analysis I -- Blatt 9

## Aufgabe 1

(a) Setze $s = \sum_{i=1}^n a_i$. Dann gilt $f'(x) = 2(nx - s)$ und $f'(x) =
    0$ genau dann, wenn $x = \frac{s}{n} =: x_0$. Ferner gilt $f''(x) = 2n$ und
    $f''(x_0) = 2n > 0$. Die Funktion $f$ besitzt also ein Minimum an der Stelle
    $x_0$.

(b) Es gilt
    $$g'(x) = \ee^{-\frac{x^2}{6}} \left(-\frac{x^4}{3} + 3x^2\right)$$
    und $g'(x_0) = 0$ genau dann, wenn $x_0 \in \set{-3, 0, 3}$.
    Ferner gilt
    $$g''(x) = \ee^{-x**2/6}\left(\frac{1}{9} x^5 - \frac{7}{3} x^3 +
    6x\right)$$
    und $g''(0) = 0, g''(-3) > 0$ und $g''(3) < 0$. Die Funktion besitzt also
    bei $x = -3$ ein Minimum und bei $x = 3$ ein Maximum.

## Aufgabe 2

Die Funktion $q \colon \R \to \R, x \mapsto x^2$ ist konvex: für $x_1, x_2 \in
\R$ und $\lambda \in (0, 1)$ sind die folgenden Aussagen äquivalent:
$$\begin{aligned}
q(\lambda x_1 + (1-\lambda) x_2) &\leqslant \lambda q(x_1) + (1 - \lambda)
q(x_2) \\
(\lambda x_1 + (1-\lambda) x_2)^2 &\leqslant \lambda x_1^2 + (1 - \lambda) x_2^2
\\
\lambda^2 x_1^2 + 2\lambda(1-\lambda)x_1x_2 + (1-\lambda)^2 x_2^2 &\leqslant
\lambda x_1^2 + (1 - \lambda) x_2^2 \\
\lambda^2 x_1^2 + 2\lambda(1-\lambda)x_1x_2 + (1-\lambda)^2 x_2^2 - \lambda
x_1^2 - (1 - \lambda) x_2^2 &\leqslant 0 \\
2\lambda(1-\lambda)x_1x_2 + \lambda x_1^2 (\lambda - 1) + (1 - \lambda) x_2^2 (1
- \lambda - 1) &\leqslant 0 \\
2\lambda(1-\lambda)x_1x_2 - \lambda(1-\lambda) x_1^2 - \lambda(1 - \lambda)
x_2^2  &\leqslant 0 \\
\lambda(1-\lambda) (2x_1x_2 - x_1^2 - x_2^2) &\leqslant 0 \\
-\lambda(1-\lambda) (x_1 - x_2)^2 &\leqslant 0 \\
\lambda(1-\lambda) (x_1 - x_2)^2 &\geqslant 0
\end{aligned}$$
Letzteres ist in jedem Fall richtig, $q$ ist also konvex. Nun folgt für $x_1,
x_2 \in [-1, 2]$ und $\lambda \in (0, 1)$:
$$\begin{aligned}
f(\lambda x_1 + (1 - \lambda) x_2) &= ((\lambda x_1 + (1 - \lambda) x_2)^2)^2 \\
&\leqslant (\lambda x_1^2 + (1 - \lambda) x_2^2)^2 \\
&\leqslant \lambda x_1^4 + (1 - \lambda) x_2^4 \\
&= \lambda f(x_1) + (1 - \lambda) f(x_2).
\end{aligned}$$

Die Ungleichung $2ab \leqslant a^2 + b^2,\;a, b \in \R$, die aus der binomischen
Formel folgt, wurde hier nicht verwendet, aber in welcher Art von Mathematik
wird denn ein Lösungsweg vorgeschrieben?

## Aufgabe 3

(a) Betrachte die Zerlegung $[0, 1] = [0, 0.25] \cup [0.25, 0.5] \cup [0.5,
    0.75] \cup [0.75, 1]$. Dann ist eine Annäherung von oben gegeben durch
    $$\int_0^1 x^2 \dd x \leqslant \sum_{i=1}^4 \frac{1}{4} \cdot
    \left(\frac{i}{4}\right)^2 = \frac{15}{32}.$$

(b) Die Zerlegung in $n$ Teilintervalle sei von der Form $[0, 1] =
    \bigcup_{i=1}^n \left[\frac{i-1}{n}, \frac{i}{n}\right]$.
    Dann gilt
    $$\int_0^1 x^2 \dd x \approx \sum_{i=1}^n \frac{1}{n} \cdot
    \left(\frac{i}{n}\right)^2 = \frac{1}{n^3} \sum_{i=1}^n i^2 =
    \frac{n(n+1)(2n+1)}{6n^3} = \frac{2n^2 + 3n + 1}{6n^2}.$$
    Für große $n$ nähert sich dieser Ausdruck dem tatsächlichen Wert des
    Integrals, nämlich $\frac{1}{3}$, an.

(c) Für alle $x \in \R$ gilt $0 \leqslant x^{39} (\sin(x))^8 \leqslant x^{39}$.
    Wegen der Monotonie des Integrals folgt
    $$0 = \int_0^1 0 \dd x \leqslant \int_0^1 x^{39} (\sin(x))^8 \dd x \leqslant
    \int_0^1 x^{39} \dd x = \frac{1}{40}.$$

## Aufgabe 4

Sei $a < b$. Betrachte die Nullfunktion $f \equiv 0$. Sie ist stetig und einmal
stetig differenzierbar, also gilt $f \in \mathcal{C}[a, b] \cap \mathcal{C}^1(a,
b)$ und $f(a) = 0 = f(b)$. Dann gilt für alle $x \in (a, b)$, dass $f'(x) = 0$,
nicht nur für eines.
