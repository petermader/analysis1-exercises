# Analysis I -- Blatt 10

## Aufgabe 1

Wir betrachten die Kontraposition: Sei $f$ nicht die Nullfunktion. Zu zeigen ist
nun, dass es eine integrierbare Funktion $g$ gibt, sodass $\int_a^b f(x) g(x)
\dd x \ne 0$.

Betrachte $g = f$. Da $f$ stetig ist, gibt es eine $\eps$-Umgebung, in der $f$
nicht verschwindet. Die Funktion $h \colon [a, b] \to \R, x \mapsto f(x) g(x)
= (f(x))^2$ ist in der obigen $\eps$-Umgebung echt positiv. Jede Treppenfunktion
$\varphi$ mit $\varphi \geqslant h$ besitzt also ein Intervall, in dem sie einen
echt positiven Wert annimmt. Somit folgt
$$0 < \overline{\int_a^b} h(x) \dd x = \int_a^b h(x) \dd x = \int_a^b f(x) g(x)
\dd x,$$
wobei $\overline{\int_\cdot^\cdot}$ das Oberintegral bedeuten soll.

## Aufgabe 2

Es gilt $R_{X,\Xi_1}f = R_{X,\Xi_2}f = 0$. Dennoch ist der Wert der Riemannsumme
von den Stützstellen abhängig: Wählt man $\Xi_3 = \set{-\frac{3}{2}, -1, 1, 2}$,
so erhält man $R_{X,\Xi_3}f = 1$.

## Aufgabe 3

Von Interesse ist nur die Differenzierbarkeit an der Stelle 0. Es gilt
$$\lim_{h\nearrow 0} \frac{f(h) - f(0)}{h - 0} = \lim_{h\nearrow 0} -h = 0$$
und
$$\lim_{h\searrow 0} \frac{f(h) - f(0)}{h - 0} = \lim_{h\searrow 0} h = 0.$$
Somit ist $f$ an der Stelle 0 differenzierbar und es gilt $f'(x) = 2 \abs{x}$.
Diese Funktion ist offensichtlich stetig, aber nicht differenzierbar.

## Aufgabe 4

Die Näherung mit der Trapezregel liefert den Wert 10. Die Riemannsumme ergibt
den Wert 11. Die Riemannsumme liegt somit näher am tatsächlichen Wert.
