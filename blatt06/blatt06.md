# Analysis I -- Blatt 6

## Aufgabe 1

(a) Die Funktionen $g_1, g_2 \colon \R \to \R$ mit $g_1(x) = 1$ und $g_2(x) = 1$
    sind offenbar stetig. Dann ist laut Satz 4.1.16 auch $g \colon \R \setminus
    \set{0} \to \R$ mit $g(x) = \frac{g_1(x)}{g_2(x)} = \frac{1}{x}$ stetig an
    allen. Da die Komposition zweier stetiger Funktionen wiederum stetig ist
    (Satz 4.1.17), ist $f$ an allen Stellen außer $x_0 = 0$ stetig. Die
    Unstetigkeit bei $x_0 = 0$ ist noch zu zeigen.

    Dazu wählen wir $\eps = \frac{1}{2}$. Dann sei $\delta > 0$ und wir wählen
    $$x = \frac{1}{2k\pi + \frac{\pi}{2}},\qquad \text{wobei} \qquad k =
    \lceil\frac{1}{2\pi\delta}\rceil \in \mathbb{Z}.$$
    Dann gilt
    $$\begin{aligned}
    & k = \lceil\frac{1}{2\pi\delta}\rceil > \frac{1}{2\pi\delta} - \frac{1}{4}
    = \frac{1}{2\pi\delta} - \frac{\pi}{2 \cdot 2\pi} =
    \frac{\frac{1}{\delta} - \frac{\pi}{2}}{2\pi} \\
    \iff & 2k\pi > \frac{1}{\delta} - \frac{\pi}{2} \\
    \iff & \frac{1}{x} = 2k\pi + \frac{\pi}{2} > \frac{1}{\delta} \\
    \iff & \abs{x} = x < \delta,
    \end{aligned}$$
    aber
    $$\abs{f(x) - f(x_0)} = \abs{\sin\left(\frac{1}{x}\right)} =
    \abs{\sin\left(2k\pi + \frac{\pi}{2}\right)} = 1 > \frac{1}{2} = \eps.$$

(b) Wieder ist die Komposition und die punktweise Multiplikation zweier stetiger
    Funktionen stetig, weshalb nur die Stetigkeit an der Stelle $x_0 = 0$
    gezeigt werden muss.

    Dazu sei $\eps > 0$ und $\delta = \eps$. Ferner sei $x \in \R$ mit $\abs{x -
    x_0} = \abs{x} < \delta$. Ist $x = 0$, so gilt trivialerweise
    $$\abs{f(x) - f(x_0)} = 0 < \eps.$$
    Sei es gelte deshalb $x \ne 0$. Dann haben wir
    $$\abs{f(x) - f(x_0)} = \abs{x \sin\left(\frac{1}{x}\right)} =
    \abs{x}\underbrace{\abs{\sin\left(\frac{1}{x}\right)}}_{\leqslant 1} <
    \delta = \eps.$$

(c) Setze $\alpha = 3$ und $\beta = \frac{4}{\mathrm{e}^2}$.

## Aufgabe 2

(a) Sei $K \in \R$ eine obere und untere Schranke von $f$. Es gelte also
    $\abs{f(x)} \leqslant K$ für $x \in \R$. Ist $K = 0$, so ist auch $g = 0$
    stetig. Sei deshalb fortan $K > 0$.

    Nun sei $\eps > 0$. Dann wählen wir $\delta = \frac{\eps}{K}$. Für $x \in
    \R$ mit $\abs{x} < \delta$ gilt dann:
    $$\abs{g(x) - g(0)} = \abs{xf(x)} = \abs{x}
    \underbrace{\abs{f(x)}}_{\leqslant K} < \delta K = \eps.$$

(b) Sei $x_0 \in \Q$ und $\eps > 0$. Wähle $\delta = \frac{1}{2}\abs{\sqrt{2} -
    \abs{x_0}}$. Dann sei $x \in \R$ mit $\abs{x - x_0} < \delta$.

    * Gilt $\abs{x_0} < \sqrt{2}$, so folgt
      $$\begin{aligned}
      \abs{x} & = \abs{x} - \delta + \delta = \abs{x - x_0 + x_0} - \delta +
      \delta \\
      & \leqslant \abs{x - x_0} + \abs{x_0} - \delta + \delta =
      \underbrace{\abs{x - x_0} - \delta}_{< 0} + \, \delta + \abs{x_0} \\
      & < \delta + \abs{x_0} = \frac{1}{2}\abs{\sqrt{2} - \abs{x_0}} +
      \abs{x_0} \\
      & = \frac{1}{2}(\sqrt{2} - \abs{x_0}) + \abs{x_0} =
      \frac{1}{2}(\sqrt{2} + \abs{x_0}) \\
      & < \frac{1}{2}(\sqrt{2} + \sqrt{2}) = \sqrt{2}.
      \end{aligned}$$
      Also haben wir
      $$\abs{h(x) - h(x_0)} = \abs{0 - 0} = 0 < \eps.$$
    * Gilt $\abs{x_0} > \sqrt{2}$, so folgt
      $$\begin{aligned}
      \abs{x} &= \abs{x} + \delta - \delta > \abs{x} + \abs{x_0 - x} - \delta \\
      & \geqslant \abs{x + x_0 - x} - \delta = \abs{x_0} - \delta \\
      &= \abs{x_0} - \frac{1}{2}\lvert\underbrace{\sqrt{2} - \abs{x_0}}_{<
      0}\rvert =
      \abs{x_0} - \frac{1}{2}(\abs{x_0} - \sqrt{2}) \\
      &= \frac{\sqrt{2}}{2} + \frac{\abs{x_0}}{2} > \frac{\sqrt{2} +
      \sqrt{2}}{2} = \sqrt{2}.
      \end{aligned}$$
      Also haben wir
      $$\abs{h(x) - h(x_0)} = \abs{1 - 1} = 0 < \eps.$$

## Aufgabe 3

(a) $f$ ist stetig und es gilt $f(-1) = - 1,\;f(1) = 1$. Laut Nullstellensatz
    (Satz 4.2.1) muss $f$ daher eine Nullstelle in $[-1, 1]$ besitzen. Ferner
    gibt es laut Satz 4.3.2 eine Maximal- und eine Minimalstelle von $f$.

(b) Die Funktion $g' \colon \R \to \R, x \mapsto x$, also $g' = \mathrm{id}_\R$
    ist offenbar stetig. Da laut Satz 4.1.16 das punktweise Produkt von stetigen
    Funktionen wieder stetig ist, ist auch $g = g' \cdot g'$ stetig. $g$ ist
    aber nicht gleichmäßig stetig: wähle $\eps = 1$. Dann sei $\delta > 0$.
    Wähle dann $x_0 = \frac{1}{\delta},\; x = x_0 + \frac{\delta}{2}$. Dann gilt
    $$\abs{x - x_0} = \abs{\frac{1}{\delta} + \frac{\delta}{2} -
    \frac{1}{\delta}} = \frac{\delta}{2} < \delta,$$
    aber
    $$\abs{f(x) - f(x_0)} = \abs{\left(\frac{1}{\delta} +
    \frac{\delta}{2}\right)^2 - \frac{1}{\delta^2}} = \abs{\frac{1}{\delta^2} +
    2 \cdot \frac{1}{\delta} \cdot \frac{\delta}{2} + \frac{\delta^2}{4} -
    \frac{1}{\delta^2}} = \abs{1 + \frac{\delta^2}{4}} > 1 = \eps.$$

## Aufgabe 4

(a) Es gilt
    $$z_1 = (3+\ii)(2-\ii)(4+\ii)^{-1} = (7-\ii)\left(\frac{4}{17} -
    \frac{\ii}{17}\right) = \frac{27}{17} - \frac{11}{17}\ii$$
    und
    $$z_2 = \frac{1 + 4\ii}{-2\ii} = -2 + \frac{1}{2}\ii.$$

(b) Es gilt $z = \overline{(4+2\ii + z_3) \cdot z_4} =
    \overline{\left(5+\frac{3}{2}\ii\right) \cdot z_4} = \overline{-\frac{1}{2}
    + \frac{5}{3}\ii} = -\frac{1}{2} - \frac{5}{3}\ii$.

    Skizze:

    \begin{center}
    \begin{tikzpicture}[scale=1.5]
        \draw[black!15!white,step=0.25] (-2.49, -2.49) grid (5.49, 3.49);
        \draw[black!60!white,step=1] (-2.49, -2.49) grid (5.49, 3.49);

        % axes
        \draw[thick,->] (-2, 0) -- (5, 0) node[anchor=west] {$\R$};
        \draw[thick,->] (0, -2) -- (0, 3) node[anchor=south] {$\ii\R$};

        \fill[red] (2, 1) circle (0.7pt) node[anchor=south west] {$z_1$};
        \fill[red] (2, 0) circle (0.7pt) node[anchor=south west] {$z_2$};
        \fill[red] (1, 0.5) circle (0.7pt) node[anchor=south west] {$z_3$};
        \fill[red] (0, 0.333) circle (0.7pt) node[anchor=south west]
        {$z_4$};

        \fill[blue] (4, 2) circle (0.7pt) node[anchor=south west] {$z_2
        \cdot z_1$};
        \fill[blue] (5, 1.5) circle (0.7pt) node[anchor=south west] {$z_2
        \cdot z_1 + z_3$};
        \fill[blue] (-0.5, 1.666) circle (0.7pt) node[anchor=south
        east] {$(z_2 \cdot z_1 + z_3) \cdot z_4$};
        \fill[blue] (-0.5, -1.666) circle (0.7pt) node[anchor=south
        east] {$\overline{(z_2 \cdot z_1 + z_3) \cdot z_4}$};
    \end{tikzpicture}
    \end{center}

(c) Es gilt
    $$\begin{aligned}
    \omega^n = \begin{cases}
    \frac{1}{2} \sqrt{3} + \frac{1}{2} \ii      & \text{falls $n \equiv 1 \pmod{12}$} \\
    \frac{1}{2} + \frac{1}{2} \sqrt{3} \ii      & \text{falls $n \equiv 2 \pmod{12}$} \\
    \ii & \text{falls $n \equiv 3 \pmod{12}$} \\
    -\frac{1}{2} + \frac{1}{2} \sqrt{3} \ii     & \text{falls $n \equiv 4 \pmod{12}$} \\
    - \frac{1}{2} \sqrt{3} + \frac{1}{2} \ii    & \text{falls $n \equiv 5 \pmod{12}$} \\
    -1 & \text{falls $n \equiv 6 \pmod{12}$} \\
    - \frac{1}{2} \sqrt{3} - \frac{1}{2} \ii    & \text{falls $n \equiv 7 \pmod{12}$} \\
    -\frac{1}{2} - \frac{1}{2} \sqrt{3} \ii     & \text{falls $n \equiv 8 \pmod{12}$} \\
    -\ii & \text{falls $n \equiv 9 \pmod{12}$} \\
    \frac{1}{2} - \frac{1}{2} \sqrt{3} \ii      & \text{falls $n \equiv 10 \pmod{12}$} \\
    \frac{1}{2} \sqrt{3} - \frac{1}{2} \ii      & \text{falls $n \equiv 11 \pmod{12}$} \\
    1 & \text{falls $n \equiv 0 \pmod{12}$.}
    \end{cases}
    \end{aligned}$$

    Skizze:

    \begin{center}
    \begin{tikzpicture}[scale=2.5]
        \draw[black!15!white,step=0.25] (-1.99, -1.99) grid (1.99, 1.99);
        \draw[black!60!white,step=1] (-1.99, -1.99) grid (1.99, 1.99);

        % axes
        \draw[thick,->] (-1.5, 0) -- (1.5, 0) node[anchor=west] {$\R$};
        \draw[thick,->] (0, -1.5) -- (0, 1.5) node[anchor=south] {$\ii\R$};

        \fill[red] (0.866, 0.5) circle (0.7pt) node[anchor=south west]
        {$\omega^{12k+1}$};
        \fill[red!75!green] (0.5, 0.866) circle (0.7pt) node[anchor=south west]
        {$\omega^{12k+2}$};
        \fill[red!50!green] (0, 1) circle (0.7pt) node[anchor=south west]
        {$\omega^{12k+3}$};
        \fill[red!25!green] (-0.5, 0.866) circle (0.7pt) node[anchor=south east]
        {$\omega^{12k+4}$};
        \fill[green] (-0.866, 0.5) circle (0.7pt) node[anchor=south east]
        {$\omega^{12k+5}$};
        \fill[green!75!blue] (-1, 0) circle (0.7pt) node[anchor=south east]
        {$\omega^{12k+6}$};
        \fill[green!50!blue] (-0.866, -0.5) circle (0.7pt) node[anchor=north
        east] {$\omega^{12k+7}$};
        \fill[green!25!blue] (-0.5, -0.866) circle (0.7pt) node[anchor=north
        east] {$\omega^{12k+8}$};
        \fill[blue] (0, -1) circle (0.7pt) node[anchor=north west]
        {$\omega^{12k+9}$};
        \fill[blue!75!red] (0.5, -0.866) circle (0.7pt) node[anchor=north west]
        {$\omega^{12k+10}$};
        \fill[blue!50!red] (0.866, -0.5) circle (0.7pt) node[anchor=north west]
        {$\omega^{12k+11}$};
        \fill[blue!25!red] (1, 0) circle (0.7pt) node[anchor=south west]
        {$\omega^{12k}$};
    \end{tikzpicture}
    \end{center}
