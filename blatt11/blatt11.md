# Analysis I -- Blatt 11

## Aufgabe 1

(b) Es gilt
    $$\begin{aligned}
    \int_0^{\frac{\pi}{4}} \frac{(\cos(x))^3}{1 - \sin(x)} \dd x &=
    \int_0^{\frac{\pi}{4}} \frac{(1 - (\sin(x))^2)(\cos(x))}{1 - \sin(x)} \dd x
    =
    \int_0^{\frac{\pi}{4}} (1 + \sin(x))\cos(x) \dd x \\
    &= \int_{\sin(0)}^{\sin\left(\frac{\pi}{4}\right)} (1 + x) \dd x =
    \int_0^{\frac{\sqrt{2}}{2}} (1 + x) \dd x = \frac{1}{4} +
    \frac{\sqrt{2}}{2}.
    \end{aligned}$$

(c) Es gilt
    $$\int_0^{\frac{\pi}{6}} \sin(x)\cos(x) \dd x =
    \int_{\sin(0)}^{\sin\left(\frac{\pi}{6}\right)} x \dd x = \frac{1}{2} x^2
    \bigg|_{x = 0}^\frac{1}{2} = \frac{1}{8}.$$

## Aufgabe 2

(a) Es gilt
    $$\int_0^3 x \ee^{3x} \dd x = \frac{1}{3} x \ee^{3x} \bigg|_{x=0}^3 -
    \int_0^3 \frac{1}{3} \ee^{3x} \dd x = \ee^9 - \left(\frac{1}{9} \ee^{3x}
    \bigg|_{x=0}^3\right) = \ee^9 - \frac{1}{9} \ee^9 + \frac{1}{9} =
    \frac{8}{9} \ee^9 + \frac{1}{9}.$$

(b) Es gilt
    $$\int_1^\ee x^2 \log(x) \dd x = \frac{1}{3} x^3 \log(x) \bigg|_{x=1}^e -
    \int_1^\ee \frac{1}{x} \cdot \frac{1}{3} x^3 \dd x = \frac{1}{3} \ee^3 -
    \left(\frac{1}{9} x^3 \bigg|_{x=1}^\ee\right) = \frac{2}{9} \ee^3 +
    \frac{1}{9}.$$

(c) Es gilt
    $$\int_0^{\frac{\pi}{6}} \sin(x) \cos(x) \dd x = \sin^2(x)
    \bigg|_{x=0}^{\frac{\pi}{6}} - \int_0^{\frac{\pi}{6}} \cos(x) \sin(x) \dd
    x.$$
    Also folgt
    $$\int_0^{\frac{\pi}{6}} \sin(x) \cos(x) \dd x = \frac{1}{2}
    \left(\sin^2(x) \bigg|_{x=0}^{\frac{\pi}{6}}\right) = \frac{1}{8}.$$

## Aufgabe 3

(a) Es gilt
    $$\frac{x^2 - 2x - 1}{x^2 - 5x + 6} = 1 + \frac{2}{x - 2} + \frac{1}{x -
    3}.$$
    Nun folgt
    $$\begin{aligned}
    \int_4^5 \frac{x^2 - 2x - 1}{x^2 - 5x + 6} &= \int_4^5 \dd x + 2 \int_4^5
    \frac{1}{x-2} \dd x + \int_4^5 \frac{1}{x-3} \dd x \\
    &= 1 + 2 \int_2^3 \frac{1}{x} \dd x + \int_1^2 \frac{1}{x} \dd x = 1 +
    \log(2) + \log(3).
    \end{aligned}$$

(b) Es gilt
    $$\begin{aligned}
    \int_1^\infty \frac{\log(x)}{x^{1+\alpha}} \dd x &=
    \lim_{b\to\infty} \int_1^b \frac{\log(x)}{x^{1+\alpha}} \dd x \\
    &= \lim_{b\to\infty} \left(- \frac{\log(x)}{\alpha x^\alpha} \bigg|_{x=1}^b
    - \int_1^b -\frac{1}{\alpha x^{1+\alpha}} \dd x\right) \\
    &= \lim_{b\to\infty} \left(- \frac{\log(x)}{\alpha x^\alpha} -
    \frac{1}{\alpha} \left(x^{-\alpha} \bigg|_{x=1}^b\right)\right) \\
    &= \lim_{b\to\infty} \left(- \frac{\log(x)}{\alpha x^\alpha} -
    \frac{1}{\alpha^2} (b^{-\alpha} - 1)\right) \\
    &= \lim_{b\to\infty} \left(\frac{1}{\alpha^2} - \frac{\log(b)}{\alpha
    b^\alpha} - \frac{1}{\alpha^2b^\alpha}\right) = \frac{1}{\alpha^2}.
    \end{aligned}$$

(c) Betrachtet man $F \colon \R \to \R, x \mapsto \log(x) + 2\arctan(x)$, so
    gilt
    $$\frac{\dd}{\dd x} F(x) = \frac{1}{x} + \frac{2}{x^2 + 1} = \frac{x^2 + 2x
    + 1}{x(x^2 + 1)} = f(x).$$
    Somit ist $F$ eine Stammfunktion.

## Aufgabe 4

(a) Das Integral existiert und es gilt:
    $$\int_0^\infty \ee^{-x} \dd x = \lim_{b \to \infty} \int_0^b \ee^{-x} \dd x
    = \lim_{b\to\infty} \left(-\ee^{-x} \bigg|_{x=0}^b\right) =
    \lim_{b\to\infty} -\ee^{-b} + 1 = 1.$$

(b) Das Integral existiert und es gilt:
    $$\int_0^1 \frac{1}{\sqrt{x}} \dd x = \lim_{a\to 0} \int_a^1
    x^{-\frac{1}{2}} \dd x = \lim_{a\to 0} \left(2x^{\frac{1}{2}} \bigg|_{x=a}^1
    \right) = \lim_{a \to 0} (2 - 2\sqrt{a}) = 2.$$

(c) Die Folge $(a_n)_{n\in\N}$ mit $a_n = 2(\sqrt{n} - \sqrt{2})$ ist nach oben
    unbeschränkt und daher divergent. Deshalb existiert das Integral nicht, denn
    dann wäre
    $$\int_2^\infty \frac{1}{\sqrt{x}} \dd x = \lim_{b\to\infty}
    \left(2\sqrt{x} \bigg|_{x=2}^b\right) = \lim_{b\to\infty} 2(\sqrt{b} -
    \sqrt{2}) = \infty.$$

(d) Das Integral existiert und es gilt mit der Substitutionsregel:
    $$\begin{aligned}
    \int_2^\infty \frac{x}{(1-x^2)^2} \dd x &= \lim_{b\to\infty} -\frac{1}{2}
    \int_2^b \frac{-2x}{(1-x^2)^2} \dd x \\
    &= \lim_{b\to\infty} -\frac{1}{2} \int_{-3}^{1-b^2} \frac{1}{x^2} \dd x \\
    &= \lim_{b\to\infty} -\frac{1}{2} \left(-\frac{1}{x}
    \bigg|_{x=-3}^{1-b^2}\right) \\
    &= \lim_{b\to\infty} -\frac{1}{2} \left(-\frac{1}{1-b^2} -
    \frac{1}{3}\right) = \frac{1}{6}.
    \end{aligned}$$
