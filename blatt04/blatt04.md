# Analysis I -- Blatt 4

## Aufgabe 1

(a) **Induktionsanfang**: mit $n = 1$ gilt
    $$(1 + x)^n = 1 + x \geqslant 1 + x = 1 + nx.$$

    **Induktionsannahme**: Sei $n > 1$. Die Behauptung gelte nun für $n - 1$.

    **Induktionsschritt**: unter Anwendung der Induktionsannahme gilt:
    $$\begin{aligned}
    (1 + x)^n &= \underbrace{(1 + x)}_{\geqslant 0}\underbrace{(1 +
    x)^{n-1}}_{\geqslant 0} \\
    &\geqslant (1 + x)(1 + (n - 1)x) \\
    &= 1 + (n - 1)x + x + \underbrace{(n - 1)x^2}_{\geqslant 0} \\
    &\geqslant 1 + (n - 1)x + x = 1 + nx.
    \end{aligned}$$

(b) **Induktionsanfang**: mit $n = 1$ gilt:
    $$(1 + x)^n = 1 + x + 0 = 1 + nx + \frac{1}{2}n(n - 1)x^2.$$

    **Induktionsannahme**: Sei $n > 1$. Die Behauptung gelte nun für $n - 1$.

    **Induktionsschritt**: unter Anwendung der Induktionsannahme gilt:
    $$\begin{aligned}
    (1 + x)^n &= (1 + x)(1 + x)^{n-1} \geqslant (1 + x)\left(1 + (n-1)x +
    \frac{1}{2}(n-1)(n-2)x^2\right) \\
    &= 1 + (n-1)x + \frac{1}{2}(n-1)(n-2)x^2 + x + (n-1)x^2 +
    \frac{1}{2}(n-1)(n-2)x^3 \\
    &= 1 + nx + (n-1)x^2\left(1 + \frac{1}{2}(n-2) + \frac{1}{2}(n-2)x\right) \\
    &= 1 + nx + \frac{1}{2}(n-1)x^2(2 + n - 2 + \underbrace{(n - 2)x}_{\geqslant
    0}) \\
    &\geqslant 1 + nx + \frac{1}{2}n(n-1)x^2.
    \end{aligned}$$

## Aufgabe 2

(a) Da die genannten Teilfolgen konvergieren, gibt es für jedes $\eps > 0$ ein
    $n_0$, sodass für $n, m \geqslant n_0$ gilt:
    $$\abs{a_{2n} - a_{2m}} < \eps,\qquad \abs{a_{2n+1} - a_{2m+1}} <
    \eps\qquad\text{und}\qquad \abs{a_{3n} - a_{3m}} < \eps.$$

    Sei nun $\eps > 0$. Setze $m_0 = 2n_0$. Nun seien $n, m \geqslant m_0$.

    * Sind $n$ und $m$ gerade, gibt es $n', m' \geqslant n_0$ mit $n = 2n', m =
      2m'$ und wegen der ersten Ungleichung gilt
      $$\abs{a_n - a_m} = \abs{a_{2n'} - a_{2m'}} < \eps.$$
    * Sind $n$ und $m$ ungerade, gibt es $n', m' \geqslant n_0$ mit $n = 2n' +
      1, m = 2m' + 1$ und wegen der ersten Ungleichung gilt
      $$\abs{a_n - a_m} = \abs{a_{2n' + 1} - a_{2m' + 1}} < \eps.$$
    * Sind die Paritäten verschieden, so sei ohne Einschränkung $n$ gerade und
      $m$ ungerade. Definiere
      $$n' = \begin{cases}n & \text{falls $n$ durch 3 teilbar,} \\
      n + 2 & \text{falls $n + 2$ durch 3 teilbar,} \\
      n + 4 & \text{sonst,}\end{cases} \qquad m' = \begin{cases}
      m & \text{falls $m$ durch 3 teilbar,} \\
      m + 2 & \text{falls $m + 2$ durch 3 teilbar,} \\
      m + 4 & \text{sonst.}
      \end{cases}$$
      Dann sind $n'$ und $m'$ durch 3 teilbar, $n'$ ist gerade und $m'$
      ungerade. Nun gilt
      $$\abs{a_{n'} - a_n} < \eps, \qquad \abs{a_{m'} - a_m} < \eps \qquad
      \text{und} \qquad \abs{a_{n'} - a_{m'}} < \eps.$$
      Es folgt
      $$\begin{aligned}
      \abs{a_n - a_m} &= \abs{(a_n - a_{n'}) + (a_{n'} - a_m)} \\
      &\leqslant \abs{a_n - a_{n'}} + \abs{a_{n'} - a_m} \\
      &< \eps + \abs{(a_{n'} - a_{m'}) + (a_{m'} - a_m)} \\
      &\leqslant \eps + \abs{a_{n'} - a_{m'}} + \abs{a_{m'} -
      a_m} < 3\eps.
      \end{aligned}$$

    Somit ist $(a_n)$ eine Cauchy-Folge und damit in $\R$ konvergent.

(b) Die Folge $(a_n)_{n\in\N}$ mit $a_n = (-1)^n$ besitzt die konvergenten
    Teilfolgen $(a_{2n}) = (1, 1, 1, \dots)$ und $(a_{2n+1}) = (-1, -1, -1,
    \dots)$. $(a_n)$ selbst konvergiert aber nicht. Die Aussage ist also falsch.

## Aufgabe 3

(a) Mit $a_k = \frac{k^2}{2^k}$ gilt:
    $$\abs{\frac{a_{n+1}}{a_n}} = \frac{(k+1)^2}{2k^2} = \frac{1}{2} \cdot
    \left(1 + \frac{2}{k} + \frac{1}{k^2}\right) \leqslant \frac{8}{9} < 1,
    \qquad k \geqslant 3.$$
    Laut Quotientenkriterium konvergiert nun $\Sigma a$ absolut.
(b) Für die Folge $(b_k)$ mit $b_k = \frac{(k+1)^k}{k^{k+1}}$ gilt:
    $$\begin{aligned}
    \lim_{k\to\infty} b_k &= \lim_{k\to\infty} \frac{(k+1)^k}{k^{k+1}} \\
    &= \lim_{k\to\infty} \frac{(k+1)^{k+1}}{(k+1)k^{k+1}} \\
    &= \lim_{k\to\infty} \frac{1}{k+1}\left(\frac{k+1}{k}\right)^{k+1} \\
    &= \lim_{k\to\infty} \underbrace{\frac{1}{k+1}}_{\to 0} \underbrace{\left(1
    + \frac{1}{k}\right)^{k+1}}_{\to \mathrm{e}} = 0.
    \end{aligned}$$
    Ferner gilt $b_k \geqslant 0$ für $k \in \N$. Laut Leibniz-Kriterium
    konvergiert somit die Reihe $\sum_{k=1}^{\infty} (-1)^k b_k$ und damit auch
    $\sum_{k=1}^\infty (-1)^{k-1} b_k$. Die Reihe konvergiert jedoch nicht
    absolut.
(c) Für die Folge $(c_k)$ mit $c_k = \frac{\sqrt{k} - \sqrt{k-1}}{k}$ gilt:
    $$\abs{c_k} = \frac{\sqrt{k} - \sqrt{k-1}}{k} = \frac{k - (k -
    1)}{k(\sqrt{k} + \sqrt{k-1})} \leqslant \frac{1}{k^{\frac{3}{2}}}.$$
    Da die Reihe $\Sigma \frac{1}{k^\alpha}$ mit $\alpha = \frac{3}{2}$
    konvergiert (Satz 3.4.9), folgt laut Majorantenkriterium, dass auch $\Sigma
    c$ konvergiert. Alle Folgenglieder von $c$ sind positiv, also konvergiert
    $\Sigma c$ sogar absolut.

## Aufgabe 4

(a) Sei $\eps > 0$. Wähle $n_0 > \frac{1}{\eps} - 1$. Nun sei $n \geqslant n_0$.
    Es gilt:
    $$\begin{aligned}
    & \frac{1}{\eps} - 1 < n \\
    \iff & \frac{1}{\eps} < n + 1 \\
    \iff & 1 < \eps (n + 1) \\
    \iff & 1 < n\eps + \eps \\
    \iff & 1 - n\eps - \eps < 0 \\
    \iff & n + 1 - n\eps - \eps < n \\
    \iff & (n + 1)(1 - \eps) < n \\
    \iff & 1 - \eps < \frac{n}{n + 1} \\
    \iff & 1 - \frac{n}{n+1} < \eps.
    \end{aligned}$$

    Nun seien $n, m \geqslant n_0$. Ohne Einschränkung sei $m \geqslant n$. Nun
    folgt:
    $$\abs{a_m - a_n} = \abs{\frac{m}{m+1} - \frac{n}{n+1}} = \frac{m}{m+1} -
    \frac{n}{n+1} \leqslant 1 - \frac{n}{n+1} < \eps.$$
(b) Betrachte $(b_n)$ mit $b_n = (-1)^n$. Mit $\eps = \frac{1}{2}$ gilt für
    jedes $n_0 \in \N$:
    $$\abs{b_{n_0} - b_{n_0 + 1}} = \abs{(-1)^{n_0} - (-1)^{n_0 + 1}} = 2 >
    \eps.$$
    Somit ist $(b_n)$ keine Cauchy-Folge.
